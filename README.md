# Readme for systemdtl
Systemdtl is a package that provides a systemctl interface to fool simple things that make hard-coded requests to a systemctl executable.
This script attempts to convert every request to a suitable real command.

# DEPRECATED
As of 2021-06-11, systemdtl is now maintained in Devuan upstream directly in package [systemctl-service-shim](https://pkginfo.devuan.org/cgi-bin/policy-query.html?c=package&q=systemctl-service-shim&x=submit). And that package's source is maintained at [https://git.devuan.org/devuan/systemctl-service-shim](https://git.devuan.org/devuan/systemctl-service-shim).

# Project license
CC-BY-SA 4.0

# Dependencies
bgscripts-core for framework.sh
