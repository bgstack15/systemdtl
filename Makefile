# File: Makefile for systemdtl
# Location: 
# Author: bgstack15
# Startdate: 2020-01-13
# Title: Makefile for systemdtl source package
# Purpose: To use traditional Unix make utility
# History:
# Usage:
# Reference:
#    bgscripts Makefile, beyond-the-titanic Makefile
# Improve:
# Document:
# Dependencies:

APPNAME    = systemctl
PKGNAME    = systemdtl
APPVERSION = 0.0.1
PKGDIR	  = $(CURDIR)
SRCDIR     = $(CURDIR)/src
prefix     = /usr
SBINDIR    = $(DESTDIR)$(prefix)/sbin
SHAREDIR	  = $(DESTDIR)$(prefix)/share
CONFIGDIR  = $(DESTDIR)/etc
#APPDIR    = $(SHAREDIR)/$(APPNAME)
#APPSDIR	  = $(SHAREDIR)/applications
#DOCDIR	  = $(SHAREDIR)/doc/$(APPNAME)
#LICENSEDIR = $(SHAREDIR)/licenses/$(APPNAME)

awkbin     :=$(shell which awk)
cpbin      :=$(shell which cp)
echobin    :=$(shell which echo)
findbin    :=$(shell which find)
grepbin    :=$(shell which grep)
installbin :=$(shell which install)
rmbin      :=$(shell which rm)
sedbin     :=$(shell which sed)
sortbin    :=$(shell which sort)
truebin    :=$(shell which true)

all:
	${echobin} No compilation required.

.PHONY: clean install install_files uninstall list

list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | ${awkbin} -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | ${sortbin} | ${grepbin} -E -v -e '^[^[:alnum:]]' -e '^$@$$'

install: install_files

install_files: 
	${echobin} Installing files to ${DESTDIR}
	${installbin} -d ${SBINDIR} ${CONFIGDIR}
	${installbin} -m0755 ${SRCDIR}/sbin/${APPNAME} ${SBINDIR}
	${installbin} -m0644 ${SRCDIR}/etc/${PKGNAME}.conf ${CONFIGDIR}
	for word in $$( ${findbin} ${SRCDIR} ! -type d ! -name '*.*' -print ) ; do ${echobin} ${installbin} -m0644 $${word} ${APPDIR} ; done

uninstall:
	${echobin} SRCDIR=${SRCDIR}
	# remove all files whose path match the files from the src/ directory from this install location.
	for word in $$( ${findbin} ${DESTDIR} -mindepth 1 ! -type d -printf '%p\n' | ${awkbin} '{print length, $$0 }' | ${sortbin} -rn | ${awkbin} '{print $$2}' ) ; do ${rmbin} $${word} 2>/dev/null || ${truebin} ; done

	# remove all installed directories that are now blank.
	for word in $$( ${findbin} ${DESTDIR} -mindepth 1 -type d -printf '%p\n' | ${awkbin} '{print length, $$0 }' | ${sortbin} -rn | ${awkbin} '{print $$2}' ) ; do ${findbin} $${word} -mindepth 1 1>/dev/null 2>&1 | read 1>/dev/null 2>&1 || { rmdir "$${word}" 2>/dev/null || ${truebin} ; } ; done

clean:
	${echobin} "target $@ not implemented yet! Gotta say unh."
